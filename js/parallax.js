var skroller;
var slides = document.getElementsByClassName("slide");
var nslides = slides.length;

function init() {


  // count slides

  // height of one single slide (css: 100vh)
  sheight = slides[0].clientHeight;
  
  // Set data-values to every second slide (first, third, ...). Slides are
  // numbered sindex = 0, 1, 2, 3, ...
  //
  // data-0="top: Bpx;" data-A="top: Cpx;" where
  //   A: scrollable nr of pixels 
  //        = sheight * (nslides - 1)
  //   B: top of the slide when scroll position is 0. 1st, 3rd, 5th, ...
  //      slide are directly positioned under each other without spacing
  //        = sindex * sheight / 2
  //   C: = A / 2 + B
  var a = sheight * (nslides - 1);
  for (var sindex = 0; sindex < nslides; sindex += 2) {
    var b = sindex * sheight / 2;
    var c = a / 2 + b;
    slides[sindex].setAttribute("data-0", "top: " + b + "px;");
    slides[sindex].setAttribute("data-" + a, "top: " + c + "px;");
    slides[sindex].style.top =  b + "px";
    slides[sindex].style.zIndex = 0;
  }

  // Position odd numbered slides (#1 = 2nd, #3 = 4th, ...) in normal
  // distances to each other, as if the other slides where in between them.
  // => #1: top = sheight = 100vh, #3: top = sheight * 3 = 300vh
  for (var sindex = 1; sindex < nslides; sindex += 2) {
    slides[sindex].style.top = sindex + "00vh";
    slides[sindex].style.zIndex = 10;
  }

  console.log("#slides: " + nslides + ", height: " + sheight);

  skroller = skrollr.init();
}

function goto(link, sindex) {
  this.skroller.animateTo(sindex * sheight, {duration: 700, easing: "swing"} );
  link.focus();
}

